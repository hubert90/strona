<!DOCTYPE HTML>
<html lang="pl">

<head>
	<meta charset="UTF-8">
	<title>MENU</title>  
	<meta name="description" content="####">  
	<meta name="keywords" content="####">
	<meta name="author" content="####">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="shortcut icon" href="####">  
	<link rel="stylesheet" href="style.css" type="text/css">

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Oxygen:wght@400;700&display=swap" rel="stylesheet">
</head>

<body onclick="hideMenu()">         

<a href="http://146.59.61.204/M/g.php">Repozytorium GIT</a>  

	<header class="header">   
		<h1>MENU</h1>   
	</header>  

	<nav class="nav">


	
  

		<ul class="nav-list">
			
			<li  onmouseover="showMenu('div-psy', 'ps')"> 
			<a    href="" id="ps">Psy</a>  </li>                                                                                       
			<li onmouseover="showMenu('div-koty', 'ko')"  >          
			<a  href="" id="ko">Koty</a>       </li>       
      
			<li onmouseover="showMenu('div-gryzonie', 'gr')"> 
			<a href="" id="gr">Gryzonie</a>       </li>   
			
			<li onmouseover="showMenu('div-ptaki', 'pt')"> 
			<a  href="" id="pt">Ptaki</a>       </li>   
			
			<li onmouseover="showMenu('div-akwarystyka', 'ak')"> 
			<a  href="" id="ak">Akwarystyka</a>       </li>    

			<li onmouseover="showMenu('div-gady', 'ga')"> 
			<a  href="" id="ga">Gady i płazy</a>       </li>  
			
			<li onmouseover="showMenu('div-konie', 'kon')"> 
			<a  href="" id="kon">Konie</a>       </li>  
			
			<li onmouseover="showMenu('div-zwierzeta', 'zw')"> 
			<a  href="" id="zw">Zwierzęta gospodarskie</a>       </li>   
			
			<li onmouseover="showMenu('div-dom', 'do')"> 
			<a  href="" id="do">Dom</a>       </li>       

		</ul>          

		    

				<div id="div-psy" >                  
					<div id="divv-psy-kategorie">  
						<ul> 
							<li> <a onmouseover="resetElementPsP()" onmouseout ="changeVisibilityPsP()" href="polecane.php">Polecane ></a> </li>  
							<li> <a onmouseover="resetElementPsK()"onmouseout ="changeVisibilityPsK()" href="">Karma dla psów ></a> </li>         
							<li> <a onmouseover="resetElementPsZ()" onmouseout ="changeVisibilityPsZ()" href="">Zabawki dla psów ></a> </li>
							<li> <a onmouseover="resetElementPsKos()" onmouseout ="changeVisibilityPsKos()"  href="">Kosmetyki i pielęgnacja ></a> </li>
							<li> <a onmouseover="resetElementPsA()" onmouseout ="changeVisibilityPsA()"  href="">Akcesoria dla psów ></a> </li>
							<li> <a onmouseover="resetElementPsNa()" onmouseout ="changeVisibilityPsNa()" href="">Na spacer ></a> </li>  
							<li> <a onmouseover="resetElementPsZd()" onmouseout ="changeVisibilityPsZd()"  href="">Zdrowie ></a> </li>
							<li> <a onmouseover="resetElementPsC()" onmouseout ="changeVisibilityPsC()" href="">Czystość ></a> </li>                    
						</ul>     
					</div>    

						<div id="divv-psy-podkategorie-polecane">     
							<img class="psy-polecane" src="img/p3.png">             
						</div> 
							
						<div id="divv-psy-podkategorie-karma"> 

							<ul>
								<li> <a href="polecane.php">Karmy bytowe</a> </li> 
								<li> <a href="">Karma mokra</a> </li>
								<li> <a href="">Karma sucha</a> </li>
							</ul>   

							<ul>
								<li> <a href="">Karmy wetyraneryjne</a> </li>
								<li> <a href="">Karma mokra</a> </li>
								<li> <a href="">Karma sucha</a> </li>
							</ul>

							<ul>
								<li> <a href="">Karmy dla ras psów</a> </li>
								<li> <a href="">Beatle</a> </li>
								<li> <a href="">Boxer</a> </li>
								<li> <a href="">Buldog</a> </li>
								<li> <a href="">Cavalier</a> </li>
								<li> <a href="">Chihuachua</a> </li>
							</ul>   

							<ul>
								<li> <a href="">Przysmaki psów</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>

							</ul>

							<ul>
								<li> <a href="">Karmy specjalistyczne</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
							</ul>


						</div>    
						<div id="divv-psy-podkategorie-zabawki">

							<ul>
								<li> <a href="">Piłki</a> </li>
								<li> <a href="">przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>  
							</ul> 

							<ul>
								<li> <a href="">Frisbe</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Piłki</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>  
							</ul> 

							<ul>
								<li> <a href="">Gryzaki</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Kości</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Pozostałe</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>   
							</ul>     

						</div>  
						<div id="divv-psy-podkategorie-kosmetyki">

							<ul>
								<li> <a href="">Nożyczki, obcinacze</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Neutralizatory zapachów</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Nauka czystości</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>  
							</ul> 

							<ul>
								<li> <a href="">Odstraszacze</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Perfumy</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Szampony, odżywki</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
								<li> <a href="">Przykład 4</a> </li>  
							</ul>    

						</div>  
						<div id="divv-psy-podkategorie-akcesoria">

							<ul>
								<li> <a href="">Akcesoria</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Akcesoria</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Akcesoria</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
							</ul> 

							<ul>
								<li> <a href="">Akcesoria</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>   
							</ul>
	

						</div>   
						<div id="divv-psy-podkategorie-naspacer">

							<ul>
								<li> <a href="">Obroże, smycze</a> </li>
								<li> <a href="">Smycze zwykłe</a> </li>
								<li> <a href="">Smycze automatyczne</a> </li>
								<li> <a href="">Smycze automatyczne 2</a> </li>
								<li> <a href="">Obroże</a> </li>
							</ul>

							<ul>
								<li> <a href="">Sakiewki na pokarm</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
							</ul> 

							<ul>
								<li> <a href="">Adresówka</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>   
							</ul>

							<ul>
								<li> <a href="">Pozostałe</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>   
								<li> <a href="">Przykład 4</a> </li>   
							</ul>  

						</div>
						<div id="divv-psy-podkategorie-zdrowie">

							<ul>
								<li> <a href="">Higiena jamy ustnej</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Zdrowie oczu, uszu</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Witaminy</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
							</ul> 

							<ul>
								<li> <a href="">Leki</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Na pchły</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
							</ul>

							<ul>
								<li> <a href="">Na uspokojenie</a> </li>
								<li> <a href="">Przykład 1</a> </li>  
							</ul>    

						</div>     
						<div id="divv-psy-podkategorie-czystosc">

							<ul>  
								<li> <a href="">Kuwety, toalety</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Żwirki</a> </li>
								<li> <a href="">Zbrylające</a> </li>
								<li> <a href="">Benitowe</a> </li>
								<li> <a href="">Drewniane</a> </li>
								<li> <a href="">Silikonowe</a> </li>
							</ul>

							<ul>
								<li> <a href="">Neutralizatory zapachów</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
							</ul> 

							<ul>
								<li> <a href="">Nauka czystości</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>
								<li> <a href="">Przykład 3</a> </li>  
							</ul>

							<ul>
								<li> <a href="">Pozostałe</a> </li>
								<li> <a href="">Przykład 1</a> </li>
								<li> <a href="">Przykład 2</a> </li>    
							</ul>    

						</div>   

					<div id="divv-psy-polecane"> 
						<h5>POLECANE<h5>   
						<img class="" src="img/p.png">  
					</div>


					
					<img class="baner" src="img/b.png">
				</div>       
				





				<div id="div-koty"> 
					<div id="divv-koty-kategorie">
						<ul>
							<li> <a onmouseover="resetElementKoP()"onmouseout ="changeVisibilityKoP()" href="polecane.php">Polecane ></a> </li>  
							<li> <a onmouseover="resetElementKoK()" onmouseout ="changeVisibilityKoK()" href="">Karma dla kotów ></a> </li>         
							<li> <a onmouseover="resetElementKoZ()" onmouseout ="changeVisibilityKoZ()" href="">Zabawki dla kotów ></a> </li>
							<li> <a onmouseover="resetElementKoKos()" onmouseout ="changeVisibilityKoKos()"  href="">Kosmetyki i pielęgnacja ></a> </li>
							<li> <a onmouseover="resetElementKoA()" onmouseout ="changeVisibilityKoA()"  href="">Akcesoria dla kotów ></a> </li>
							<li> <a onmouseover="resetElementKoNa()" onmouseout ="changeVisibilityKoNa()" href="">Na spacer ></a> </li>  
							<li> <a onmouseover="resetElementKoZd()" onmouseout ="changeVisibilityKoZd()"  href="">Zdrowie ></a> </li>
							<li> <a onmouseover="resetElementKoC()" onmouseout ="changeVisibilityKoC()" href="">Czystość ></a> </li>                  
						</ul>  
					</div>    

					<div id="divv-koty-podkategorie-polecane">
					 	<img class="koty-polecane" src="img/p3.png">             
					</div> 
						
					<div id="divv-koty-podkategorie-karma">

						<ul>
							<li> <a href="">Karmy bytowe</a> </li>
							<li> <a href="">Karma mokra</a> </li>
							<li> <a href="">Karma sucha</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Karmy wetyraneryjne</a> </li>
							<li> <a href="">Karma mokra</a> </li>
							<li> <a href="">Karma sucha</a> </li>
						</ul>

						<ul>
							<li> <a href="">Karmy dla ras kotów</a> </li>
							<li> <a href="">Bengalski</a> </li>
							<li> <a href="">Brytyjski</a> </li>
							<li> <a href="">Main coon</a> </li>
							<li> <a href="">Perski</a> </li>
							<li> <a href="">Norweski</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Przysmaki kotów</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>

						</ul>  

						<ul>
							<li> <a href="">Karmy specjalistyczne</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>


					</div>    
					<div id="divv-koty-podkategorie-zabawki">

						<ul>
							<li> <a href="">Wędki</a> </li>
							<li> <a href="">przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul> 

						<ul>
							<li> <a href="">Piłeczki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Myszki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul> 

						<ul>
							<li> <a href="">Wędki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Piłeczki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Pozostałe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>   
						</ul>     

					</div>  
					<div id="divv-koty-podkategorie-kosmetyki">

						<ul>
							<li> <a href="">Nożyczki, obcinacze</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Neutralizatory zapachów</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Nauka czystości</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul> 

						<ul>
							<li> <a href="">Odstraszacze</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Perfumy</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Szampony, odżywki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>    

					</div>  
					<div id="divv-koty-podkategorie-akcesoria">

						<ul>
							<li> <a href="">Miski</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Stojaki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li> 
						</ul>

						<ul>
							<li> <a href="">Legowiska</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul> 

						<ul>
							<li> <a href="">Transportery</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>
 

					</div>   
					<div id="divv-koty-podkategorie-naspacer">

						<ul>
							<li> <a href="">Obroże, smycze</a> </li>
							<li> <a href="">Smycze zwykłe</a> </li>
							<li> <a href="">Smycze automatyczne</a> </li>
							<li> <a href="">Smycze automatyczne 2</a> </li>
							<li> <a href="">Obroże</a> </li>
						</ul>

						<ul>
							<li> <a href="">Sakiewki na pokarm</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul> 

						<ul>
							<li> <a href="">Adresówka</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>   
						</ul>

						<ul>
							<li> <a href="">Pozostałe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>   
							<li> <a href="">Przykład 4</a> </li>   
						</ul>  

					</div>
					<div id="divv-koty-podkategorie-zdrowie">

						<ul>  
							<li> <a href="">Higiena jamy ustnej</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Zdrowie oczu, uszu</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Witaminy</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul> 

						<ul>
							<li> <a href="">Leki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Na pchły</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

						<ul>
							<li> <a href="">Na uspokojenie</a> </li>
							<li> <a href="">Przykład 1</a> </li>  
						</ul>    

					</div>     
					<div id="divv-koty-podkategorie-czystosc">

						<ul>  
							<li> <a href="">Kuwety, toalety</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Żwirki</a> </li>
							<li> <a href="">Zbrylające</a> </li>
							<li> <a href="">Benitowe</a> </li>
							<li> <a href="">Drewniane</a> </li>
							<li> <a href="">Silikonowe</a> </li>
						</ul>

						<ul>
							<li> <a href="">Neutralizatory zapachów</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul> 

						<ul>
							<li> <a href="">Nauka czystości</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Pozostałe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>    
						</ul>    

					</div>   
					<div id="divv-koty-polecane"> 
						<h5>POLECANE<h5>   
						<img class="" src="img/p.png">  
					</div>


					
					<img class="baner" src="img/b.png">
				</div>     


				<div id="div-gryzonie"> 
					<div id="divv-gryzonie-kategorie">
						<ul>
							<li> <a onmouseover="resetElementGrP()" onmouseout ="changeVisibilityGrP()" href="polecane.php">Polecane ></a> </li>  
							<li> <a onmouseover="resetElementGrK()" onmouseout ="changeVisibilityGrK()" href="">Karma ></a> </li>         
							<li> <a onmouseover="resetElementGrZ()" onmouseout ="changeVisibilityGrZ()" href="">Zabawki ></a> </li>
							<li> <a onmouseover="resetElementGrKos()" onmouseout ="changeVisibilityGrKos()"  href="">Kosmetyki i pielęgnacja ></a> </li>
							<li> <a onmouseover="resetElementGrA()" onmouseout ="changeVisibilityGrA()"  href="">Akcesoria ></a> </li>
							<!-- <li> <a onmouseover="resetElementGrNa()" onmouseout ="changeVisibilityGrNa()" href="">Na spacer ></a> </li>   -->
							<li> <a onmouseover="resetElementGrZd()" onmouseout ="changeVisibilityGrZd()"  href="">Zdrowie ></a> </li>
							<li> <a onmouseover="resetElementGrC()" onmouseout ="changeVisibilityGrC()" href="">Czystość ></a> </li>                    
						</ul>    
					</div>    

					<div id="divv-gryzonie-podkategorie-polecane">
					 	<img class="gryzonie-polecane" src="img/p3.png">             
					</div> 
						
					<div id="divv-gryzonie-podkategorie-karma">

						<ul>
							<li> <a href="">Dla chomika</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Dla koszatniczki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>

						</ul>

						<ul>
							<li> <a href="">Dla królika</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>
						
						<ul>
							<li> <a href="">Dla myszy</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

						<ul>
							<li> <a href="">Przysmaki</a> </li>
							<li> <a href="">Kolby</a> 
						</ul>  


					</div>    
					<div id="divv-gryzonie-podkategorie-zabawki">

						<ul>
							<li> <a href="">Kostki mineralne</a> </li>
							<li> <a href=""></a> </li>
							<li> <a href="">Przykład 2</a> </li> 
						</ul> 

						<ul>
							<li> <a href="">Wapienka</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

						<ul>
							<li> <a href="">Kołowrotki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul> 
 

					</div>  
					<div id="divv-gryzonie-podkategorie-kosmetyki">

						<ul>
							<li> <a href="">Szampony</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Neutralizatory zapachów</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

						<ul>
							<li> <a href="">Odżywki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul> 


					</div>  
					<div id="divv-gryzonie-podkategorie-akcesoria">

						<ul>
							<li> <a href="">Domki</a> </li>
							<li> <a href="">Legowiska</a> </li>
							<li> <a href="">Klatki</a> </li>
						</ul>

						<ul>
							<li> <a href="">Miski</a> </li>
							<li> <a href="">Poidełka 1</a> </li>
							<li> <a href="">Przykład 2</a> </li> 
						</ul>

						<ul>
							<li> <a href="">Transportery</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul> 

						<ul>
							<li> <a href="">Tunele</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>
 

					</div>   
					<div id="divv-gryzonie-podkategorie-naspacer">

					</div>
					<div id="divv-gryzonie-podkategorie-zdrowie">

						<ul>
							<li> <a href="">Witaminy </a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Higiena</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>

						</ul>

						<ul>
							<li> <a href="">Leki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>    

					</div>     
					<div id="divv-gryzonie-podkategorie-czystosc">

						<ul>  
							<li> <a href="">Kuwety, toalety</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Neutralizatory zapachów</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul> 

						<ul>
							<li> <a href="">Ściółki, piaski, żwirki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Pozostałe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>    
						</ul>    

					</div>   
					<div id="divv-gryzonie-polecane"> 
						<h5>POLECANE<h5>   
						<img class="" src="img/p.png">  
					</div>


					
					<img class="baner" src="img/b.png">
				</div>     





  


				<div id="div-ptaki"> 
					<div id="divv-ptaki-kategorie">
						<ul>
							<li> <a onmouseover="resetElementPtP()" onmouseout ="changeVisibilityPtP()" href="polecane.php">Polecane ></a> </li>  
							<li> <a onmouseover="resetElementPtK()" onmouseout ="changeVisibilityPtK()" href="">Karma ></a> </li>         
							<!-- <li> <a onmouseover="resetElementPtZ()" onmouseout ="changeVisibilityPtZ()" href="">Zabawki dla psów ></a> </li>
							<li> <a onmouseover="resetElementPtKos()" onmouseout ="changeVisibilityPtKos()"  href="">Kosmetyki i pielęgnacja ></a> </li> -->
							<li> <a onmouseover="resetElementPtA()" onmouseout ="changeVisibilityPtA()"  href="">Akcesoria dla psów ></a> </li>
							<!-- <li> <a onmouseover="resetElementPtNa()" onmouseout ="changeVisibilityPtNa()" href="">Na spacer ></a> </li>   -->
							<li> <a onmouseover="resetElementPtZd()" onmouseout ="changeVisibilityPtZd()"  href="">Zdrowie ></a> </li>
							<li> <a onmouseover="resetElementPtC()" onmouseout ="changeVisibilityPtC()" href="">Czystość ></a> </li>                  
						</ul>  
					</div>    

					<div id="divv-ptaki-podkategorie-polecane">
					 	<img class="ptaki-polecane" src="img/p3.png">             
					</div> 
						
					<div id="divv-ptaki-podkategorie-karma">

						<ul>
							<li> <a href="">Dla dużych papug</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Dla kanarka</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>

						</ul>

						<ul>
							<li> <a href="">Dla nierozłączek</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Dla papużki falistej</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>

						</ul>

						<ul>
							<li> <a href="">Dla gołębi</a> </li>
							<li> <a href="">Kolby</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>


					</div>    
					<div id="divv-ptaki-podkategorie-zabawki">  

					</div>  
					<div id="divv-ptaki-podkategorie-kosmetyki">
 

					</div>  
					<div id="divv-ptaki-podkategorie-akcesoria">

						<ul>
							<li> <a href="">Karmidełka</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>

						</ul>

						<ul>
							<li> <a href="">Poidełka</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li> 
						</ul>

						<ul>
							<li> <a href="">Klatki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul> 

					</div>   
					<div id="divv-ptaki-podkategorie-naspacer">

					</div>
					<div id="divv-ptaki-podkategorie-zdrowie">

						<ul>
							<li> <a href="">Higiena</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Witaminy</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>    

					</div>     
					<div id="divv-ptaki-podkategorie-czystosc">

						<ul>  
							<li> <a href="">Piaski</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Pozostałe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>
 

					</div>   
					<div id="divv-ptaki-polecane"> 
						<h5>POLECANE<h5>   
						<img class="" src="img/p.png">  
					</div>


					
					<img class="baner" src="img/b.png">
				</div>     





  
				<div id="div-akwarystyka"> 
					<div id="divv-akwarystyka-kategorie">
						<ul>
							<li> <a onmouseover="resetElementAkP()" onmouseout ="changeVisibilityAkP()" href="polecane.php">Polecane ></a> </li>  
							<li> <a onmouseover="resetElementAkK()" onmouseout ="changeVisibilityAkK()" href="">Pokarm dla rybek ></a> </li>         
							<!-- <li> <a onmouseover="resetElementAkZ()" onmouseout ="changeVisibilityAkZ()" href="">Zabawki dla psów ></a> </li>
							<li> <a onmouseover="resetElementAkKos()" onmouseout ="changeVisibilityAkKos()"  href="">Aksmetyki i pielęgnacja ></a> </li> -->
							<li> <a onmouseover="resetElementAkA()" onmouseout ="changeVisibilityAkA()"  href="">Akcesoria dla rybek ></a> </li>
							<!-- <li> <a onmouseover="resetElementAkNa()" onmouseout ="changeVisibilityAkNa()" href="">Na spacer ></a> </li>   -->
							<li> <a onmouseover="resetElementAkZd()" onmouseout ="changeVisibilityAkZd()"  href="">Zdrowie ></a> </li>
							<li> <a onmouseover="resetElementAkC()" onmouseout ="changeVisibilityAkC()" href="">Czystość ></a> </li>                  
						</ul>  
					</div>    

					<div id="divv-akwarystyka-podkategorie-polecane">
					 	<img class="akwarystyka-polecane" src="img/p3.png">             
					</div> 
						
					<div id="divv-akwarystyka-podkategorie-karma">

						<ul>
							<li> <a href="">Dla ryb dennych</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Dla ryb mięsożernych</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

						<ul>
							<li> <a href="">Dla ryb roślinożernych</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Dla glonojadów</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>

						</ul>

						<ul>
							<li> <a href="">Dla krewetek</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

						<ul>
							<li> <a href="">Dla ryb tropikalnych</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>


					</div>    
					<div id="divv-akwarystyka-podkategorie-zabawki">   

					</div>  
					<div id="divv-akwarystyka-podkategorie-kosmetyki">

					</div>  
					<div id="divv-akwarystyka-podkategorie-akcesoria">

						<ul>
							<li> <a href="">Czyściki</a> </li>
							<li> <a href="">Siatki</a> </li>
							<li> <a href="">Termometry</a> </li>
							<li> <a href="">Testy wody</a> </li>  			<li> <a href="">Podłoża</a> </li>  
							<li> <a href="">Inne</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Napowietrzacze</a> </li>
							<li> <a href="">Wkłady filtrów</a> </li>
							<li> <a href="">Filtry zewnętrzne</a> </li>
							<li> <a href="">Filtry wewnętrzne</a> </li>  
							<li> <a href="">Sterylizatory</a> </li>
						</ul>

						<ul>
							<li> <a href="">Grzałki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li> 
						</ul> 

						<ul>
							<li> <a href="">Akwaria</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>
 

					</div>   
					<div id="divv-akwarystyka-podkategorie-naspacer">
					<ul>
							<li> <a href="">Akwaria</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>
					</div>
					<div id="divv-akwarystyka-podkategorie-zdrowie">

						<ul>
							<li> <a href="">Korekta ph</a> </li>
							<li> <a href="">Uzdatnianie wody</a> </li>
						</ul>

						<ul>
							<li> <a href="">Preparaty na glony</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

						<ul>
							<li> <a href="">Baktery i biostartery</a> </li>
							<li> <a href="">Przykład 1</a> </li>
						</ul>  

					</div>     
					<div id="divv-akwarystyka-podkategorie-czystosc">

						<ul>  
							<li> <a href="">Nawozy</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Odżywki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>

						<ul>
							<li> <a href="">Pozostałe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>
					</div>  

					<div id="divv-akwarystyka-polecane"> 
						<h5>POLECANE<h5>   
						<img class="" src="img/p.png">  
					</div>


					<img class="baner" src="img/b.png">
				</div>      





  
				<div id="div-gady"> 
					<div id="divv-gady-kategorie">
						<ul>
							<li> <a onmouseover="resetElementGaP()" onmouseout ="changeVisibilityGaP()" href="polecane.php">Polecane ></a> </li>  
							<li> <a onmouseover="resetElementGaK()" onmouseout ="changeVisibilityGaK()" href="">Karma dla gadów i płazów ></a> </li>         
							<li> <a onmouseover="resetElementGaZ()" onmouseout ="changeVisibilityGaZ()" href="">Wyposażenie ></a> </li>
							<!-- <li> <a onmouseover="resetElementGaKos()" onmouseout ="changeVisibilityGaKos()"  href="">Kosmetyki i pielęgnacja ></a> </li> -->
							<li> <a onmouseover="resetElementGaA()" onmouseout ="changeVisibilityGaA()"  href="">Akcesoria ></a> </li>
							<!-- <li> <a onmouseover="resetElementGaNa()" onmouseout ="changeVisibilityGaNa()" href="">Na spacer ></a> </li>  
							<li> <a onmouseover="resetElementGaZd()" onmouseout ="changeVisibilityGaZd()"  href="">Zdrowie ></a> </li> -->
							<li> <a onmouseover="resetElementGaC()" onmouseout ="changeVisibilityGaC()" href="">Czystość ></a> </li>                  
						</ul>  
					</div>    

					<div id="divv-gady-podkategorie-polecane">
					 	<img class="gady-polecane" src="img/p3.png">             
					</div> 
						
					<div id="divv-gady-podkategorie-karma">

						<ul>
							<li> <a href="">Dla żółwia</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Dla krabów</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Dla kameleona</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Dla jaszczurki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Dla żółwi wodnych</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Dla gekona</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   


					</div>    
					<div id="divv-gady-podkategorie-zabawki">

						<ul>
							<li> <a href="">Kryjówki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>     
						
						<ul>
							<li> <a href="">Miski</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Pęsety </a> </li>
							<li> <a href="">Łopatki</a> </li>
							<li> <a href="">Haki</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Przyrządy pomiarowe</a> </li>
							<li> <a href="">Przykład 1</a> </li>

						</ul>   

						<ul>
							<li> <a href="">Wodospady</a> </li>
							<li> <a href="">Generatory pary</a> </li>
							<li> <a href="">Skraplacze</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Termostaty</a> </li>
							<li> <a href="">Wyłączniki czasowe</a> </li>
						</ul>   

					</div>  
					<div id="divv-gady-podkategorie-kosmetyki">   

					</div>  
					<div id="divv-gady-podkategorie-akcesoria">

						<ul>
							<li> <a href="">Terraria</a> </li>
							<li> <a href="">Tła do terrarium</a> </li>
							<li> <a href="">Przykład Filtry</a> </li>
							<li> <a href="">Dekoracje</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Kamienie grzewcze</a> </li>
							<li> <a href="">Maty grzewcze</a> </li>
							<li> <a href="">Promienniki</a> </li>
						</ul>

						<ul>
							<li> <a href="">Żarówki</a> </li>
							<li> <a href="">Dzienne</a> </li>
							<li> <a href="">Nocne</a> </li>						
							<li> <a href="">Ośw. UVB</a> </li>
						</ul>
 
					</div>   
					<div id="divv-gady-podkategorie-naspacer">

					</div>
					<div id="divv-gady-podkategorie-zdrowie">  

					</div>     
					<div id="divv-gady-podkategorie-czystosc">   
						<ul>
							<li> <a href="">Podłoża piaskowe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul> 

						<ul>
							<li> <a href="">Podłoża torfowe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul> 

						<ul>
							<li> <a href="">Podłoża kukurydziane</a> </li>
							<li> <a href="">Drewniane</a> </li>
							<li> <a href="">Wiórowe</a> </li> 
						</ul>

						<ul>
							<li> <a href="">Podłoża wernikulitowe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>  
						</ul>  

						<ul>
							<li> <a href="">Podłoża kokosowe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>  
						</ul>  

						<ul>
							<li> <a href="">Podłoża mchowe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>  
						</ul>  
					</div>   

					<div id="divv-gady-polecane"> 
						<h5>POLECANE<h5>   
						<img class="" src="img/p.png">  
					</div>


					
					<img class="baner" src="img/b.png">
				</div>     





  
				<div id="div-konie"> 
					<div id="divv-konie-kategorie">
						<ul>
							<li> <a onmouseover="resetElementKonP()" onmouseout ="changeVisibilityKonP()" href="polecane.php">Polecane ></a> </li>  
							<li> <a onmouseover="resetElementKonK()" onmouseout ="changeVisibilityKonK()" href="">Pasza dla koni ></a> </li>         
							<!-- <li> <a onmouseover="resetElementKonZ()" onmouseout ="changeVisibilityKonZ()" href="">Zabawki dla psów ></a> </li> -->
							<li> <a onmouseover="resetElementKonKos()" onmouseout ="changeVisibilityKonKos()"  href="">Kosmetyki i pielęgnacja ></a> </li>
							<li> <a onmouseover="resetElementKonA()" onmouseout ="changeVisibilityKonA()"  href="">Akcesoria dla koni ></a> </li>
							<!-- <li> <a onmouseover="resetElementKonNa()" onmouseout ="changeVisibilityKonNa()" href="">Na spacer ></a> </li>   -->
							<li> <a onmouseover="resetElementKonZd()" onmouseout ="changeVisibilityKonZd()"  href="">Zdrowie ></a> </li>
							<!-- <li> <a onmouseover="resetElementKonC()" onmouseout ="changeVisibilityKonC()" href="">Czystość ></a> </li>                   -->
						</ul>  
					</div>    

					<div id="divv-konie-podkategorie-polecane">
					 	<img class="konie-polecane" src="img/p3.png">             
					</div> 
						
					<div id="divv-konie-podkategorie-karma">

						<ul>
							<li> <a href="">Pasza dla koni</a> </li>
							<li> <a href=""></a> </li>
							<li> <a href="">Dla źrębiąt</a> </li>		<li> <a href="">Musli</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Granulaty</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

						<ul>
							<li> <a href="">Mesz</a> </li>
							<li> <a href="">Mekuch</a> </li>
							<li> <a href="">Otręby</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Objętościowe</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>

						</ul>

						<ul>
							<li> <a href="">Smakołyki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

					</div>    
					<div id="divv-konie-podkategorie-zabawki">    

					</div>  
					<div id="divv-konie-podkategorie-kosmetyki">

						<ul>
							<li> <a href="">Maści dla koni</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>  
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Szampony dla koni</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Żele dla koni</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul> 

						<ul>
							<li> <a href="">Kopyta</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

						<ul>
							<li> <a href="">Stomatologia koni</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li> 
						</ul>

						<ul>
							<li> <a href="">Szczotki </a> </li>
							<li> <a href="">Zgrzebła</a> </li>

						</ul>    

					</div>  
					<div id="divv-konie-podkategorie-akcesoria">

						<ul>
							<li> <a href="">Sondy żołądkowe</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li> 
						</ul>

						<ul>
							<li> <a href="">Żłoby</a> </li>
							<li> <a href="">Wiadra</a> </li>
							<li> <a href="">Miski</a> </li> 
						</ul>

						<ul>
							<li> <a href="">Kantary</a> </li>
							<li> <a href="">Uwiązy</a> </li>
						</ul> 

					</div>   
					<div id="divv-konie-podkategorie-naspacer"> 

					</div>
					<div id="divv-konie-podkategorie-zdrowie">

						<ul>
							<li> <a href="">Kopyta</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

						<ul>
							<li> <a href="">Układ Oddechowy</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
						</ul>

						<ul>
							<li> <a href="">Przeciwbólowe</a> </li>
							<li> <a href="">Przeciwzapalne</a> </li>
							<li> <a href="">Witaminy</a> </li>
						</ul> 

						<ul>
							<li> <a href="">Uspokojenie</a> </li>
							<li> <a href="">Wyciszenie</a> </li>
						</ul>

						<ul>
							<li> <a href="">Elektrolity</a> </li>
							<li> <a href="">Wydolność</a> </li>  
						</ul>    

					</div>     
					<div id="divv-konie-podkategorie-czystosc"> 

					</div>   
					<div id="divv-konie-polecane"> 
						<h5>POLECANE<h5>   
						<img class="" src="img/p.png">  
					</div>


					
					<img class="baner" src="img/b.png">
				</div>     





  
				<div id="div-zwierzeta"> 
					<div id="divv-zwierzeta-kategorie">
						<ul>
							<li> <a onmouseover="resetElementZwP()" onmouseout ="changeVisibilityZwP()" href="polecane.php">Zwierzęta gospodarskie - polecane ></a> </li>  
							<li> <a onmouseover="resetElementZwK()" onmouseout ="changeVisibilityZwnK()" href="">Drób ></a> </li>         
							<li> <a onmouseover="resetElementZwZ()" onmouseout ="changeVisibilityZwZ()" href="">Trzoda chlewna ></a> </li>
							<li> <a onmouseover="resetElementZwKos()" onmouseout ="changeVisibilityZwnKos()"  href="">Bydło ></a> </li>
							<!-- <li> <a onmouseover="resetElementZwA()" onmouseout ="changeVisibilityZwA()"  href="">Akcesoria dla psów ></a> </li>
							<li> <a onmouseover="resetElementZwNa()" onmouseout ="changeVisibilityZwNa()" href="">Na spacer ></a> </li>  
							<li> <a onmouseover="resetElementZwZd()" onmouseout ="changeVisibilityZwZd()"  href="">Zdrowie ></a> </li>
							<li> <a onmouseover="resetElementZwC()" onmouseout ="changeVisibilityZwC()" href="">Czystość ></a> </li>                   -->
						</ul>  
					</div>       

					<div id="divv-zwierzeta-podkategorie-polecane">
					 	<img class="zwierzeta-polecane" src="img/p3.png">             
					</div> 
						
					<div id="divv-zwierzeta-podkategorie-karma">

						<ul>
							<li> <a href="">Karmy/pasze</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Akcesoria</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>

						<ul>
							<li> <a href="">Zdrowie</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Pielęgnacja</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>

						</ul>

						<ul>
							<li> <a href="">Czystość</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>


					</div>    
					<div id="divv-zwierzeta-podkategorie-zabawki">

					<ul>
							<li> <a href="">Karmy/pasze</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Akcesoria</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>

						<ul>
							<li> <a href="">Zdrowie</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Pielęgnacja</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>

						</ul>

						<ul>
							<li> <a href="">Czystość</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>     

					</div>  
					<div id="divv-zwierzeta-podkategorie-kosmetyki">

						<ul>
							<li> <a href="">Nożyczki, obcinacze</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Neutralizatory zapachów</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Nauka czystości</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul> 

						<ul>
							<li> <a href="">Odstraszacze</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Perfumy</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Szampony, odżywki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul>    

					</div>  
					<div id="divv-zwierzeta-podkategorie-akcesoria">

					<ul>
							<li> <a href="">Karmy/pasze</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Akcesoria</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>

						<ul>
							<li> <a href="">Zdrowie</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Pielęgnacja</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>

						</ul>

						<ul>
							<li> <a href="">Czystość</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>
 

					</div>   
					<div id="divv-zwierzeta-podkategorie-naspacer">

					</div>
					<div id="divv-zwierzeta-podkategorie-zdrowie">

					</div>     
					<div id="divv-zwierzeta-podkategorie-czystosc">

					</div>   
					<div id="divv-zwierzeta-polecane"> 
						<h5>POLECANE<h5>   
						<img class="" src="img/p.png">  
					</div>


					
					<img class="baner" src="img/b.png">
				</div>     





  
				<div id="div-dom"> 
					<div id="divv-dom-kategorie">
						<ul>
							<li> <a onmouseover="resetElementDoP()" onmouseout ="changeVisibilityDoP()" href="polecane.php">Polecane ></a> </li>  
							<li> <a onmouseover="resetElementDoK()" onmouseout ="changeVisibilityDoK()" href="">Produkty konopne ></a> </li>         
							<li> <a onmouseover="resetElementDoZ()" onmouseout ="changeVisibilityDoZ()" href="">BROS ></a> </li>
							<li> <a onmouseover="resetElementDoKos()" onmouseout ="changeVisibilityDoKos()"  href="">BIOPON ></a> </li>
							<li> <a onmouseover="resetElementDoA()" onmouseout ="changeVisibilityDoA()"  href="">CURVER ></a> </li>
							<li> <a onmouseover="resetElementDoNa()" onmouseout ="changeVisibilityDoNa()" href="">FISKARS ></a> </li>  
							<!-- <li> <a onmouseover="resetElementDoZd()" onmouseout ="changeVisibilityDoZd()"  href="">Zdrowie ></a> </li>
							<li> <a onmouseover="resetElementDoC()" onmouseout ="changeVisibilityDoC()" href="">Czystość ></a> </li>                    -->
						</ul>  
					</div>    
 
					<div id="divv-dom-podkategorie-polecane">
					 	<img class="dom-polecane" src="img/p3.png">             
					</div> 
						
					<div id="divv-dom-podkategorie-karma">

					<ul>
							<li> <a href="">Olejki CBD dla zwierząt</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Olejki CBD dla ludzi</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>

						<ul>
							<li> <a href="">Olejki CBG dla ludzi</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>
						</ul>   

						<ul>
							<li> <a href="">Susz konopny</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>

						</ul>

						<ul>
							<li> <a href="">Żywność konopna</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>


					</div>    
					<div id="divv-dom-podkategorie-zabawki">

						<ul>
							<li> <a href="">Kleszcze</a> </li>
							<li> <a href="">przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul> 

						<ul>
							<li> <a href="">Muchy</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Komary</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul> 

						<ul>
							<li> <a href="">Osy</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Mole</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Mrówki</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>     

					</div>  
					<div id="divv-dom-podkategorie-kosmetyki">

						<ul>
							<li> <a href="">Środki do pielęgnacji</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
							<li> <a href="">Przykład 4</a> </li>  
						</ul> 

					</div>  
					<div id="divv-dom-podkategorie-akcesoria">

						<ul>
							<li> <a href="">Stylowyy dom</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Ogród</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li> 
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Warsztat</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>
 
					</div>   
					<div id="divv-dom-podkategorie-naspacer">

					<ul>
							<li> <a href="">Kuchnia</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li>
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

						<ul>
							<li> <a href="">Ogród</a> </li>
							<li> <a href="">Przykład 1</a> </li>
							<li> <a href="">Przykład 2</a> </li> 
							<li> <a href="">Przykład 3</a> </li>  
						</ul>

					</div>
					<div id="divv-dom-podkategorie-zdrowie">   

					</div>     
					<div id="divv-dom-podkategorie-czystosc"> 

					</div>   
					<div id="divv-dom-polecane"> 
						<h5>POLECANE<h5>   
						<img class="" src="img/p.png">  
					</div>


					
					<img class="baner" src="img/b.png">
				</div>     
   





  



	</nav>  
	






	<main class="main">
    
	</main> 

	
	<footer class="footer">
		
	</footer>  



	<script src="scripts.js" type="text/javascript"></script>   

</body>

</html>