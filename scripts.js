	// MENU GLOWNE
function showMenu(x, y) { 

	const element = document.getElementById(x);
	const elementcol = document.getElementById(y);

		if (element.style.visibility  == 'hidden') {
			element.style.visibility = 'visible';  
			element.style.zIndex = 99; 
			elementcol.style.color = "#34d459";   
		}
		else {    
			element.style.visibility = 'hidden';
			element.style.zIndex = 0;   
			elementcol.style.color = '#2a2b2e'; 
			
		}     
}   

function hideMenu() {
	document.getElementById('div-psy').style.visibility = 'hidden';
	document.getElementById('ps').style.color = '#2a2b2e';
	document.getElementById('div-koty').style.visibility = 'hidden';
	document.getElementById('ko').style.color = '#2a2b2e';
	document.getElementById('div-gryzonie').style.visibility = 'hidden';
	document.getElementById('gr').style.color = '#2a2b2e';
	document.getElementById('div-ptaki').style.visibility = 'hidden';
	document.getElementById('pt').style.color = '#2a2b2e';
	document.getElementById('div-akwarystyka').style.visibility = 'hidden';
	document.getElementById('ak').style.color = '#2a2b2e';
	document.getElementById('div-gady').style.visibility = 'hidden';
	document.getElementById('ga').style.color = '#2a2b2e';
	document.getElementById('div-konie').style.visibility = 'hidden';
	document.getElementById('kon').style.color = '#2a2b2e';
	document.getElementById('div-zwierzeta').style.visibility = 'hidden';
	document.getElementById('zw').style.color = '#2a2b2e';
	document.getElementById('div-dom').style.visibility = 'hidden';
	document.getElementById('do').style.color = '#2a2b2e';              
}  






	// PSY
	function changeVisibilityPsP() {  
		document.getElementById("divv-psy-podkategorie-polecane").style.display = "none";       
	}
	function resetElementPsP() {
		document.getElementById("divv-psy-podkategorie-polecane").style.display = "grid";     
	}     
	function changeVisibilityPsK() { 
		document.getElementById("divv-psy-podkategorie-karma").style.display = "none";     
	}
	function resetElementPsK() {
		document.getElementById("divv-psy-podkategorie-karma").style.display = "grid";     
	}  
	function changeVisibilityPsZ() { 
		document.getElementById("divv-psy-podkategorie-zabawki").style.display = "none";     
	}
	function resetElementPsZ() {
		document.getElementById("divv-psy-podkategorie-zabawki").style.display = "grid";     
	}    
	function changeVisibilityPsKos() { 
		document.getElementById("divv-psy-podkategorie-kosmetyki").style.display = "none";     
	}
	function resetElementPsKos() {
		document.getElementById("divv-psy-podkategorie-kosmetyki").style.display = "grid";     
	}     
	function changeVisibilityPsA() { 
		document.getElementById("divv-psy-podkategorie-akcesoria").style.display = "none";     
	}
	function resetElementPsA() {
		document.getElementById("divv-psy-podkategorie-akcesoria").style.display = "grid";     
	}     
	function changeVisibilityPsNa() { 
		document.getElementById("divv-psy-podkategorie-naspacer").style.display = "none";       
	}
	function resetElementPsNa() {
		document.getElementById("divv-psy-podkategorie-naspacer").style.display = "grid";       
	}     
	function changeVisibilityPsZd() { 
		document.getElementById("divv-psy-podkategorie-zdrowie").style.display = "none";     
	}
	function resetElementPsZd() {
		document.getElementById("divv-psy-podkategorie-zdrowie").style.display = "grid";     
	}  
	function changeVisibilityPsC() { 
		document.getElementById("divv-psy-podkategorie-czystosc").style.display = "none";     
	}
	function resetElementPsC() {
		document.getElementById("divv-psy-podkategorie-czystosc").style.display = "grid";         
	}     

	//  KOTY
	function changeVisibilityKoP() {  
		document.getElementById("divv-koty-podkategorie-polecane").style.display = "none";       
	}
	function resetElementKoP() {
		document.getElementById("divv-koty-podkategorie-polecane").style.display = "grid";     
	}     
	function changeVisibilityKoK() { 
		document.getElementById("divv-koty-podkategorie-karma").style.display = "none";     
	}
	function resetElementKoK() {
		document.getElementById("divv-koty-podkategorie-karma").style.display = "grid";     
	}  
	function changeVisibilityKoZ() { 
		document.getElementById("divv-koty-podkategorie-zabawki").style.display = "none";     
	}
	function resetElementKoZ() {
		document.getElementById("divv-koty-podkategorie-zabawki").style.display = "grid";     
	}    
	function changeVisibilityKoKos() { 
		document.getElementById("divv-koty-podkategorie-kosmetyki").style.display = "none";     
	}
	function resetElementKoKos() {
		document.getElementById("divv-koty-podkategorie-kosmetyki").style.display = "grid";     
	}     
	function changeVisibilityKoA() { 
		document.getElementById("divv-koty-podkategorie-akcesoria").style.display = "none";     
	}
	function resetElementKoA() {
		document.getElementById("divv-koty-podkategorie-akcesoria").style.display = "grid";     
	}     
	function changeVisibilityKoNa() { 
		document.getElementById("divv-koty-podkategorie-naspacer").style.display = "none";       
	}
	function resetElementKoNa() {
		document.getElementById("divv-koty-podkategorie-naspacer").style.display = "grid";       
	}     
	function changeVisibilityKoZd() { 
		document.getElementById("divv-koty-podkategorie-zdrowie").style.display = "none";     
	}
	function resetElementKoZd() {
		document.getElementById("divv-koty-podkategorie-zdrowie").style.display = "grid";     
	}  
	function changeVisibilityKoC() { 
		document.getElementById("divv-koty-podkategorie-czystosc").style.display = "none";     
	}
	function resetElementKoC() {
		document.getElementById("divv-koty-podkategorie-czystosc").style.display = "grid";       
	}  


		//  GRYZONIE
		function changeVisibilityGrP() {  
			document.getElementById("divv-gryzonie-podkategorie-polecane").style.display = "none";       
		}
		function resetElementGrP() {
			document.getElementById("divv-gryzonie-podkategorie-polecane").style.display = "grid";     
		}     
		function changeVisibilityGrK() { 
			document.getElementById("divv-gryzonie-podkategorie-karma").style.display = "none";     
		}
		function resetElementGrK() {
			document.getElementById("divv-gryzonie-podkategorie-karma").style.display = "grid";     
		}  
		function changeVisibilityGrZ() { 
			document.getElementById("divv-gryzonie-podkategorie-zabawki").style.display = "none";     
		}
		function resetElementGrZ() {
			document.getElementById("divv-gryzonie-podkategorie-zabawki").style.display = "grid";     
		}    
		function changeVisibilityGrKos() { 
			document.getElementById("divv-gryzonie-podkategorie-kosmetyki").style.display = "none";     
		}
		function resetElementGrKos() {
			document.getElementById("divv-gryzonie-podkategorie-kosmetyki").style.display = "grid";     
		}     
		function changeVisibilityGrA() { 
			document.getElementById("divv-gryzonie-podkategorie-akcesoria").style.display = "none";     
		}
		function resetElementGrA() {
			document.getElementById("divv-gryzonie-podkategorie-akcesoria").style.display = "grid";     
		}     
		function changeVisibilityGrNa() { 
			document.getElementById("divv-gryzonie-podkategorie-naspacer").style.display = "none";       
		}
		function resetElementGrNa() {
			document.getElementById("divv-gryzonie-podkategorie-naspacer").style.display = "grid";       
		}     
		function changeVisibilityGrZd() { 
			document.getElementById("divv-gryzonie-podkategorie-zdrowie").style.display = "none";     
		}
		function resetElementGrZd() {
			document.getElementById("divv-gryzonie-podkategorie-zdrowie").style.display = "grid";     
		}  
		function changeVisibilityGrC() { 
			document.getElementById("divv-gryzonie-podkategorie-czystosc").style.display = "none";     
		}
		function resetElementGrC() {
			document.getElementById("divv-gryzonie-podkategorie-czystosc").style.display = "grid";       
		}  

			//  PTAKI
	function changeVisibilityPtP() {  
		document.getElementById("divv-ptaki-podkategorie-polecane").style.display = "none";       
	}
	function resetElementPtP() {
		document.getElementById("divv-ptaki-podkategorie-polecane").style.display = "grid";     
	}     
	function changeVisibilityPtK() { 
		document.getElementById("divv-ptaki-podkategorie-karma").style.display = "none";     
	}
	function resetElementPtK() {
		document.getElementById("divv-ptaki-podkategorie-karma").style.display = "grid";     
	}  
	function changeVisibilityPtZ() { 
		document.getElementById("divv-ptaki-podkategorie-zabawki").style.display = "none";     
	}
	function resetElementPtZ() {
		document.getElementById("divv-ptaki-podkategorie-zabawki").style.display = "grid";     
	}    
	function changeVisibilityPtKos() { 
		document.getElementById("divv-ptaki-podkategorie-kosmetyki").style.display = "none";     
	}
	function resetElementPtKos() {
		document.getElementById("divv-ptaki-podkategorie-kosmetyki").style.display = "grid";      
	}     
	function changeVisibilityPtA() { 
		document.getElementById("divv-ptaki-podkategorie-akcesoria").style.display = "none";     
	}
	function resetElementPtA() {
		document.getElementById("divv-ptaki-podkategorie-akcesoria").style.display = "grid";     
	}     
	function changeVisibilityPtNa() { 
		document.getElementById("divv-ptaki-podkategorie-naspacer").style.display = "none";       
	}
	function resetElementPtNa() {
		document.getElementById("divv-ptaki-podkategorie-naspacer").style.display = "grid";       
	}     
	function changeVisibilityPtZd() { 
		document.getElementById("divv-ptaki-podkategorie-zdrowie").style.display = "none";     
	}
	function resetElementPtZd() {
		document.getElementById("divv-ptaki-podkategorie-zdrowie").style.display = "grid";     
	}  
	function changeVisibilityPtC() { 
		document.getElementById("divv-ptaki-podkategorie-czystosc").style.display = "none";     
	}
	function resetElementPtC() {
		document.getElementById("divv-ptaki-podkategorie-czystosc").style.display = "grid";       
	}  

		//  AKWARYSTYKA
		function changeVisibilityAkP() {  
			document.getElementById("divv-akwarystyka-podkategorie-polecane").style.display = "none";       
		}
		function resetElementAkP() {
			document.getElementById("divv-akwarystyka-podkategorie-polecane").style.display = "grid";     
		}     
		function changeVisibilityAkK() { 
			document.getElementById("divv-akwarystyka-podkategorie-karma").style.display = "none";     
		}
		function resetElementAkK() {
			document.getElementById("divv-akwarystyka-podkategorie-karma").style.display = "grid";     
		}  
		function changeVisibilityAkZ() { 
			document.getElementById("divv-akwarystyka-podkategorie-zabawki").style.display = "none";     
		}
		function resetElementAkZ() {
			document.getElementById("divv-akwarystyka-podkategorie-zabawki").style.display = "grid";     
		}    
		function changeVisibilityAkKos() { 
			document.getElementById("divv-akwarystyka-podkategorie-kosmetyki").style.display = "none";     
		}
		function resetElementAkKos() {
			document.getElementById("divv-akwarystyka-podkategorie-kosmetyki").style.display = "grid";     
		}     
		function changeVisibilityAkA() { 
			document.getElementById("divv-akwarystyka-podkategorie-akcesoria").style.display = "none";     
		}
		function resetElementAkA() {
			document.getElementById("divv-akwarystyka-podkategorie-akcesoria").style.display = "grid";     
		}     
		function changeVisibilityAkNa() { 
			document.getElementById("divv-akwarystyka-podkategorie-naspacer").style.display = "none";       
		}
		function resetElementAkNa() {
			document.getElementById("divv-akwarystyka-podkategorie-naspacer").style.display = "grid";       
		}     
		function changeVisibilityAkZd() { 
			document.getElementById("divv-akwarystyka-podkategorie-zdrowie").style.display = "none";     
		}
		function resetElementAkZd() {
			document.getElementById("divv-akwarystyka-podkategorie-zdrowie").style.display = "grid";     
		}  
		function changeVisibilityAkC() { 
			document.getElementById("divv-akwarystyka-podkategorie-czystosc").style.display = "none";     
		}
		function resetElementAkC() {
			document.getElementById("divv-akwarystyka-podkategorie-czystosc").style.display = "grid";       
		}  

			//  GADY
	function changeVisibilityGaP() {  
		document.getElementById("divv-gady-podkategorie-polecane").style.display = "none";       
	}
	function resetElementGaP() {
		document.getElementById("divv-gady-podkategorie-polecane").style.display = "grid";     
	}     
	function changeVisibilityGaK() { 
		document.getElementById("divv-gady-podkategorie-karma").style.display = "none";     
	}
	function resetElementGaK() {
		document.getElementById("divv-gady-podkategorie-karma").style.display = "grid";     
	}  
	function changeVisibilityGaZ() { 
		document.getElementById("divv-gady-podkategorie-zabawki").style.display = "none";     
	}
	function resetElementGaZ() {
		document.getElementById("divv-gady-podkategorie-zabawki").style.display = "grid";     
	}    
	function changeVisibilityGaKos() { 
		document.getElementById("divv-gady-podkategorie-kosmetyki").style.display = "none";     
	}
	function resetElementGaKos() {
		document.getElementById("divv-gady-podkategorie-kosmetyki").style.display = "grid";     
	}     
	function changeVisibilityGaA() { 
		document.getElementById("divv-gady-podkategorie-akcesoria").style.display = "none";     
	}
	function resetElementGaA() {
		document.getElementById("divv-gady-podkategorie-akcesoria").style.display = "grid";     
	}     
	function changeVisibilityGaNa() { 
		document.getElementById("divv-gady-podkategorie-naspacer").style.display = "none";       
	}
	function resetElementGaNa() {
		document.getElementById("divv-gady-podkategorie-naspacer").style.display = "grid";       
	}     
	function changeVisibilityGaZd() { 
		document.getElementById("divv-gady-podkategorie-zdrowie").style.display = "none";     
	}
	function resetElementGaZd() {
		document.getElementById("divv-gady-podkategorie-zdrowie").style.display = "grid";     
	}  
	function changeVisibilityGaC() { 
		document.getElementById("divv-gady-podkategorie-czystosc").style.display = "none";     
	}
	function resetElementGaC() {
		document.getElementById("divv-gady-podkategorie-czystosc").style.display = "grid";       
	}  

		//  KONIE
		function changeVisibilityKonP() {  
			document.getElementById("divv-konie-podkategorie-polecane").style.display = "none";       
		}
		function resetElementKonP() {
			document.getElementById("divv-konie-podkategorie-polecane").style.display = "grid";     
		}     
		function changeVisibilityKonK() { 
			document.getElementById("divv-konie-podkategorie-karma").style.display = "none";     
		}
		function resetElementKonK() {
			document.getElementById("divv-konie-podkategorie-karma").style.display = "grid";     
		}  
		function changeVisibilityKonZ() { 
			document.getElementById("divv-konie-podkategorie-zabawki").style.display = "none";     
		}
		function resetElementKonZ() {
			document.getElementById("divv-konie-podkategorie-zabawki").style.display = "grid";     
		}    
		function changeVisibilityKonKos() { 
			document.getElementById("divv-konie-podkategorie-kosmetyki").style.display = "none";     
		}
		function resetElementKonKos() {
			document.getElementById("divv-konie-podkategorie-kosmetyki").style.display = "grid";     
		}     
		function changeVisibilityKonA() { 
			document.getElementById("divv-konie-podkategorie-akcesoria").style.display = "none";     
		}
		function resetElementKonA() {
			document.getElementById("divv-konie-podkategorie-akcesoria").style.display = "grid";     
		}     
		function changeVisibilityKonNa() { 
			document.getElementById("divv-konie-podkategorie-naspacer").style.display = "none";       
		}
		function resetElementKonNa() {
			document.getElementById("divv-konie-podkategorie-naspacer").style.display = "grid";       
		}     
		function changeVisibilityKonZd() { 
			document.getElementById("divv-konie-podkategorie-zdrowie").style.display = "none";     
		}
		function resetElementKonZd() {
			document.getElementById("divv-konie-podkategorie-zdrowie").style.display = "grid";     
		}  
		function changeVisibilityKonC() { 
			document.getElementById("divv-konie-podkategorie-czystosc").style.display = "none";     
		}
		function resetElementKonC() {
			document.getElementById("divv-konie-podkategorie-czystosc").style.display = "grid";       
		}  

			//  ZW
	function changeVisibilityZwP() {  
		document.getElementById("divv-zwierzeta-podkategorie-polecane").style.display = "none";       
	}
	function resetElementZwP() {
		document.getElementById("divv-zwierzeta-podkategorie-polecane").style.display = "grid";     
	}     
	function changeVisibilityZwK() { 
		document.getElementById("divv-zwierzeta-podkategorie-karma").style.display = "none";     
	}
	function resetElementZwK() {
		document.getElementById("divv-zwierzeta-podkategorie-karma").style.display = "grid";     
	}  
	function changeVisibilityZwZ() { 
		document.getElementById("divv-zwierzeta-podkategorie-zabawki").style.display = "none";     
	}
	function resetElementZwZ() {
		document.getElementById("divv-zwierzeta-podkategorie-zabawki").style.display = "grid";     
	}    
	function changeVisibilityZwKos() { 
		document.getElementById("divv-zwierzeta-podkategorie-kosmetyki").style.display = "none";     
	}
	function resetElementZwKos() {
		document.getElementById("divv-zwierzeta-podkategorie-kosmetyki").style.display = "grid";     
	}     
	function changeVisibilityZwA() { 
		document.getElementById("divv-zwierzeta-podkategorie-akcesoria").style.display = "none";     
	}
	function resetElementZwA() {
		document.getElementById("divv-zwierzeta-podkategorie-akcesoria").style.display = "grid";     
	}     
	function changeVisibilityZwNa() { 
		document.getElementById("divv-zwierzeta-podkategorie-naspacer").style.display = "none";       
	}
	function resetElementZwNa() {
		document.getElementById("divv-zwierzeta-podkategorie-naspacer").style.display = "grid";       
	}     
	function changeVisibilityZwZd() { 
		document.getElementById("divv-zwierzeta-podkategorie-zdrowie").style.display = "none";     
	}
	function resetElementZwZd() {
		document.getElementById("divv-zwierzeta-podkategorie-zdrowie").style.display = "grid";     
	}  
	function changeVisibilityZwC() { 
		document.getElementById("divv-zwierzeta-podkategorie-czystosc").style.display = "none";     
	}
	function resetElementZwC() {
		document.getElementById("divv-zwierzeta-podkategorie-czystosc").style.display = "grid";       
	}  

		//  dom
		function changeVisibilityDoP() {  
			document.getElementById("divv-dom-podkategorie-polecane").style.display = "none";       
		}
		function resetElementDoP() {
			document.getElementById("divv-dom-podkategorie-polecane").style.display = "grid";     
		}     
		function changeVisibilityDoK() { 
			document.getElementById("divv-dom-podkategorie-karma").style.display = "none";     
		}
		function resetElementDoK() {
			document.getElementById("divv-dom-podkategorie-karma").style.display = "grid";     
		}  
		function changeVisibilityDoZ() { 
			document.getElementById("divv-dom-podkategorie-zabawki").style.display = "none";     
		}
		function resetElementDoZ() {
			document.getElementById("divv-dom-podkategorie-zabawki").style.display = "grid";     
		}    
		function changeVisibilityDoKos() { 
			document.getElementById("divv-dom-podkategorie-kosmetyki").style.display = "none";     
		}
		function resetElementDoKos() {
			document.getElementById("divv-dom-podkategorie-kosmetyki").style.display = "grid";     
		}     
		function changeVisibilityDoA() { 
			document.getElementById("divv-dom-podkategorie-akcesoria").style.display = "none";     
		}
		function resetElementDoA() {
			document.getElementById("divv-dom-podkategorie-akcesoria").style.display = "grid";     
		}     
		function changeVisibilityDoNa() { 
			document.getElementById("divv-dom-podkategorie-naspacer").style.display = "none";       
		}
		function resetElementDoNa() {
			document.getElementById("divv-dom-podkategorie-naspacer").style.display = "grid";       
		}     
		function changeVisibilityDoZd() { 
			document.getElementById("divv-dom-podkategorie-zdrowie").style.display = "none";     
		}
		function resetElementDoZd() {
			document.getElementById("divv-dom-podkategorie-zdrowie").style.display = "grid";     
		}  
		function changeVisibilityDoC() { 
			document.getElementById("divv-dom-podkategorie-czystosc").style.display = "none";     
		}
		function resetElementDoC() {
			document.getElementById("divv-dom-podkategorie-czystosc").style.display = "grid";       
		}  
	
